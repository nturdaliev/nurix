<?php
/**
 * Created by JetBrains PhpStorm.
 * User: root
 * Date: 4/28/13
 * Time: 11:14 PM
 * To change this template use File | Settings | File Templates.
 */
namespace Nurix\CatalogBundle\Parser;

use DateTime;
use Doctrine\Bundle\DoctrineBundle\Registry;
use Doctrine\Common\Util\Debug;
use Nurix\CatalogBundle\Entity\Goods;
use Doctrine\ORM\EntityNotFoundException;
use Nurix\CatalogBundle\Entity\Characteristic;
use Nurix\CatalogBundle\Entity\CharacteristicSection;
use Nurix\CatalogBundle\Entity\CharacteristicType;
use Nurix\CatalogBundle\Entity\GoodsRepository;
use Symfony\Component\HttpFoundation\File\File;
use Sunra\PhpSimple\HtmlDomParser;

class Parser
{
    private $xlsx_service;
    /* @var Registry $doctrine_service */
    private $doctrine_service;
    private $googleParser;
    private $imageCount;

    public function __construct($doctrine_service, $xlsx_service, $googleParser, $imageCount)
    {
        $this->xlsx_service = $xlsx_service;
        $this->doctrine_service = $doctrine_service;
        $this->googleParser = $googleParser;
        $this->imageCount = $imageCount;
    }

    public function parseExcel(File $file)
    {
        $exelObj = $this->xlsx_service->load($file);
        $sheetData = $exelObj->getActiveSheet()->toArray(null, true, false, true);
        $index = 0;

        $entityManager = $this->doctrine_service->getManager();

        /** @var $goodsRepository GoodsRepository */
        $goodsRepository = $this->doctrine_service->getRepository('CatalogBundle:Goods');
        $goodsRepository->deactivateAll();
        $catalogs = $entityManager->getRepository('CatalogBundle:Catalog')->findAll();
        $goods_alias = array();

        $added_goods = 0;
        $updated_goods = 0;

        foreach ($catalogs as $catalog) {
            $goods_alias[$catalog->getId()] = explode(',', $catalog->getGoodsAlias());
        }

        foreach ($sheetData as $sheetRow) {

            $index++;
            if ($index == 1) continue;
            $article = $sheetRow['A'] ? $sheetRow['A'] : null;
            $name = $sheetRow['B'] ? $sheetRow['B'] : null;
            $last_update = new DateTime(date('Y-m-d', mktime(0, 0, 0, 1, $sheetRow['K'] - 1, 1900)));
            $price = (float)$sheetRow['L'];
            $urlYandex = $sheetRow['Q'];
            $subcatalog = null;
            if (!empty($article)) {

                foreach ($goods_alias as $catalog_id => $aliases) {
                    foreach ($aliases as $alias) {
                        if (!empty($alias))
                            if (strpos($name, $alias) !== false) {
                                $subcatalog = $entityManager->getRepository('CatalogBundle:Catalog')->find($catalog_id);
                                break;
                            }
                    }
                }

                /* @var Goods $good */
                $good = $goodsRepository->findOneByArticle($article);

                if ($good) {
                    $updated_goods++;
                    $good->setLastUpdate($last_update);
                    $good->setPrice($price);
                    $good->setActive(true);
                    if ($good->getImageId() == null) {
                        $gallery = $this->googleParser->saveImages($good->getName(), 'default', 'sonata.media.provider.image', 'goods_big', $this->imageCount);
                        if ($gallery != null)
                            $good->setImageId($gallery->getId());
                    }


                    if ($good->getCatalog() == null)
                        $good->setCatalog($subcatalog);

                    $entityManager->flush();


                } else {
                    $good = new Goods();
                    $good->setArticle($article);
                    $good->setName($name);
                    $good->setActive(true);
                    $good->setLastUpdate($last_update);
                    $good->setPrice($price);
                    $good->setCatalog($subcatalog);

                    $entityManager->persist($good);
                    $entityManager->flush();

                    $gallery = $this->googleParser->saveImages($name, 'default', 'sonata.media.provider.image', 'goods_big', $this->imageCount);

                    if ($gallery != null)
                        $good->setImageId($gallery->getId());
                    $entityManager->flush();
                    $added_goods++;

                }

                if ($urlYandex) {
                    $characters = $entityManager->getRepository('CatalogBundle:Characteristic')->findByGoodId($good->getId());
                    if (!$characters) {
                        $this->parseMailRu($urlYandex, $good);
                    }
                }
            }
        }
        return array('added' => $added_goods, 'updated' => $updated_goods);
    }

    public function parseMailRu($url, Goods $good)
    {
        if (!$good) {
            Throw new EntityNotFoundException("Товар не найден");
        }
        $html = HtmlDomParser::file_get_html($url);
        $table = $html->find('.good__charectistic__full')[0];
        $em = $this->doctrine_service->getManager();
        $sections = $table->find('h3');
        $characteristics = $table->find('table');
        for ($i = 0; $i < count($sections); $i++) {
            $section = $this->findOrCreateSection($sections[$i], $em);

            foreach ($characteristics[$i]->find('tr') as $tr) {
                $characteristic_type = $this->findOrCreateCharacteristicType($tr, $section, $em);

                $char = $this->normalize($tr->find('td', -1)->plaintext);
                $characteristic = new Characteristic();
                $characteristic->setValue($char);
                $characteristic->setCTypeId($characteristic_type);
                $characteristic->setGood($good);

                $em->persist($characteristic);
                $em->flush();
            }
        }
    }

    /**
     * @param $sectionElement
     * @param $em
     * @return CharacteristicSection
     */
    private function findOrCreateSection($sectionElement, $em)
    {
        $section_type = $this->normalize($sectionElement->plaintext);
        $charSectionRepository = $this->doctrine_service->getRepository('CatalogBundle:CharacteristicSection');
        $characteristic_section = $charSectionRepository->findOneBySectionvalue($section_type);
        if ($characteristic_section) {
            $section = $characteristic_section;
            return $section;
        } else {
            $section = new CharacteristicSection();
            $section->setSectionvalue($section_type);

            $em->persist($section);
            $em->flush();
            return $section;
        }
    }

    /**
     * @param $tr
     * @param $section
     * @param $em
     * @return CharacteristicType
     */
    private function findOrCreateCharacteristicType($tr, $section, $em)
    {
        $char_type = $this->normalize($tr->find('th > span > b', -1)->plaintext);

        $charRepository = $this->doctrine_service->getRepository('CatalogBundle:CharacteristicType');
        $characteristic_type = $charRepository->findOneByName($char_type);

        if (!$characteristic_type) {
            $characteristic_type = new CharacteristicType();
            $characteristic_type->setName($char_type);
            $characteristic_type->setSection($section);

            $em->persist($characteristic_type);
            $em->flush();
            return $characteristic_type;
        }
        return $characteristic_type;
    }

    /**
     * @param $section_type
     * @return string
     */
    private function normalize($section_type)
    {
        $section_type = mb_convert_encoding($section_type, 'UTF-8', 'windows-1251');
        $section_type = trim($section_type);
        $section_type = str_replace('&nbsp;', '', $section_type);
        return $section_type;
    }
}