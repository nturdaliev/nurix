<?php
namespace Nurix\CatalogBundle\DBAL;

use Doctrine\DBAL\Types\Type;
use Doctrine\DBAL\Platforms\AbstractPlatform;

class StickerType extends Type
{
    const ENUM_STATUS = 'sticker';

    public static function toArray()
    {
        return array('hit'=>'Хит продаж','new' => 'Новинка','price' => 'Супер-цена','shipping'=>'Бесплатная доставка');
    }

    public function getSqlDeclaration(array $fieldDeclaration, AbstractPlatform $platform)
    {
        return "ENUM('hit','new','price','shipping') COMMENT '(DC2Type:sticker)'";
    }

    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        return $value;
    }

    public function convertToDatabaseValue($value, AbstractPlatform $platform)
    {
        if ($value!= null && !in_array($value,array_keys($this->toArray()) )) {
            throw new \InvalidArgumentException("Invalid status");
        }
        return $value;
    }

    public function getName()
    {
        return self::ENUM_STATUS;
    }
}