<?php

namespace Nurix\CatalogBundle\Form\Type;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class BinClientsFormType extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('fio','text',array('label'=>'ФИО', 'required'=>true));
        $builder->add('phone','text',array('label'=>'Номер телефона', 'required'=>true));
        $builder->add('email','email',array('label'=>'email', 'required'=>true));
    }

    /**
     * Returns the name of this type.
     *
     * @return string The name of this type
     */
    public function getName()
    {
        return 'bin_clients';
    }
}